# RobotCars

RobotCars est un jeu python écrit avec PyGame qui vise à faire travailler les élèves sur la Programmation Orientée Objet. Les capacités visées sont :
- Ecrire le code d'une méthode de classe.
- Ajouter des attributs à une classe.
- Accéder aux attributs et méthodes d'une classe.
- Modifier le constructeur d'une classe.

## Installation

L'installation du jeu se fait simplement par les étapes suivantes :
- Télécharger les sources ou cloner le dépôt git.
  - `git clone <adresse_du_dépôt>`
  - `cd robotcars`
- (Créer un environnement virtuel python) -> optionnel mais conseillé.
  - `python -m venv nom_du_venv`
  - activer le venv avec la commande propre au système
    - (Linux)`source /nom_du_venv/Scripts/activate`
    - (Win) `nom_du_venv\Scripts\activate`
- Installer les paquets listés dans le requirements.txt
  - `pip install -r requirements.txt`

## Démarrage 

Il suffit d'exécuter le script `main.py` pour lancer le jeu.

## Utilisation

Il faut modifier la classe `Car` dans le fichier `actors/car.py` pour changer le comportement des voitures. La même classe est utilisée pour toutes les voitures présentes, ce afin d'illustrer la notion de l'instanciation (une même classe permet la création de plusieurs objets, chacun ayant son état propre)

Il faut ensuite sélectionner son niveau (**touches directionnelles**) et valider avec la touche **Entrée**.

La touche **Echap** permet de revenir au menu ou quitter le jeu.